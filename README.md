
## Context

This instant server is a chess site that allows players to play instantly against AIs hosted on the server.

## License

MIT © [Yannick Kirschhoffer](http://www.alcibiade.org/)


